package main.java.data;

public class InvalidDataException extends Exception {

    /**
     * The data wasn't properly formatted
     *
     * @param dataType Type of data parsed
     * @param msg      Message
     */
    public InvalidDataException(String dataType, String msg) {
        super("Invalid " + dataType + ": " + msg);
    }
}
