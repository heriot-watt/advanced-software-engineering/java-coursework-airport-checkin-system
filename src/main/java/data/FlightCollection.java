package main.java.data;

import main.java.data.model.Flight;
import main.java.data.model.Reference;

public class FlightCollection extends DataCollection {

    /**
     * Gets a new instance of a Booking object
     *
     * @return New Booking object
     */
    @Override
    protected Flight getNewInstance() {
        return new Flight();
    }

    /**
     * Get the CSV header of the Flight object
     */
    @Override
    protected String getHeader() {
        return "flight_code,destination,carrier,max_passengers,max_baggage_weight,max_baggage_dimension,departure_time," +
                "total_checks_in,total_baggage_volume,total_baggage_weight,total_extra_fees";
    }

    /**
     * Find a Flight in the collection
     *
     * @param key Key to find the flight
     * @return Found flight (Null if not found)
     */
    @Override
    public Flight find(Reference key) {
        return (Flight) super.find(key);
    }
}
