package main.java.data;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class CsvIO {

    private static final String RESOURCES_PATH = "src/main/resources/";

    /**
     * Read lines from a file
     *
     * @param filePath Path of the file
     * @return Array of parsed lines
     * @throws FileNotFoundException The file does not exist
     */
    public static ArrayList<String> read(String filePath) throws FileNotFoundException {
        try {
            return CsvIO.read(new FileInputStream(CsvIO.RESOURCES_PATH + filePath));
        } catch (FileNotFoundException e) {
            return CsvIO.read(new FileInputStream(filePath));
        }
    }

    /**
     * Read lines from any input stream
     *
     * @param in Input stream
     * @return Array of parsed lines
     */
    public static ArrayList<String> read(InputStream in) {
        Scanner sc = new Scanner(in);
        ArrayList<String> arr = new ArrayList<>();
        while (sc.hasNext()) {
            arr.add(sc.nextLine());
        }
        return arr;
    }

    /**
     * Write the elements of a parsed DataCollection object to a file
     *
     * @param elements Elements to write
     * @param filePath Path of the file
     * @throws IOException Could not write to file
     */
    public static void write(ArrayList<String> elements, String filePath) throws IOException {
        try {
            CsvIO.write(elements, new FileOutputStream(new File(CsvIO.RESOURCES_PATH + filePath)));
        } catch (IOException e) {
            CsvIO.write(elements, new FileOutputStream(new File(filePath)));
        }
    }

    /**
     * Write the elements of a parsed DataCollection object to any output stream
     *
     * @param elements Elements to write
     * @param out      Output stream
     * @throws IOException Could not write to output stream
     */
    public static void write(ArrayList<String> elements, OutputStream out) throws IOException {
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
        for (String line : elements) {
            bw.write(line);
            bw.newLine();
        }
        bw.flush();
    }
}
