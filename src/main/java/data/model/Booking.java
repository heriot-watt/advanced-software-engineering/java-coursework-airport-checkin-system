package main.java.data.model;

import main.java.data.InvalidDataException;
import main.java.data.InvalidReferenceException;

import java.util.Random;

public class Booking implements DataClass {

    private Reference refCode;

    private String lastName;
    private String flightCode;
    private Boolean checkedIn;
    private final BaggageDimension dimension;
    private final int weight;

    public Booking() {
        Random rand = new Random();
        dimension = new BaggageDimension(rand.nextFloat() * 30,
                rand.nextFloat() * 30,
                rand.nextFloat() * 30);
        weight = rand.nextInt(29) + 1;
    }

    /**
     * Reference to the booked flight object
     */
    public Flight flight;

    /**
     * Gets the reference code of the booking
     *
     * @return Reference string
     */
    @Override
    public Reference getRef() {
        return this.refCode;
    }

    /**
     * Last name of the booker
     *
     * @return Last name string
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Reference code of the booked flight
     *
     * @return Flight reference code string
     */
    public String getFlightCode() {
        return this.flightCode;
    }

    /**
     * Is this booking checked in ?
     *
     * @return True if the booking has been checked in, otherwise false
     */
    public Boolean getCheckedIn() {
        return this.checkedIn;
    }

    /**
     * Set if this booking has been checked in or not
     */
    public void setCheckedIn() {
        this.checkedIn = true;
    }

    /**
     * Dimensions of the baggage
     *
     * @return BaggageDimension object
     */
    public BaggageDimension getDimension() {
        return dimension;
    }

    /**
     * Weight of the baggage
     *
     * @return Baggage weight number
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Get the extra fees charged for the booking
     *
     * @return Extra fees
     */
    public float getExtraFees() {
        float extraFees = 0f;

        if (this.flight != null) {
            if (this.getWeight() > flight.getMaxBaggageWeight()
                    || this.dimension.height > this.flight.getMaxBaggageDimension().height
                    || this.dimension.width > this.flight.getMaxBaggageDimension().width
                    || this.dimension.depth > this.flight.getMaxBaggageDimension().depth) {
                extraFees = 10 + ((this.dimension.height + this.dimension.depth + this.dimension.width + this.weight) / 4);
            }
        }

        return extraFees;
    }

    /**
     * Parses a CSV formatted line to instantiate the attributes of the booking object
     *
     * @param line The string to parse data from
     * @throws InvalidDataException The string is not correctly formatted
     */
    @Override
    public void fromCsvString(String line) throws InvalidDataException {
        String[] elements = line.split(",");
        if (elements.length != 4) throw new InvalidDataException("Booking", "Invalid data format");
        try {
            this.refCode = new Reference(this, elements[0]);
        } catch (InvalidReferenceException e) {
            throw new InvalidDataException("Booking", e.getMessage());
        }

        this.lastName = elements[1];
        this.flightCode = elements[2];
        this.checkedIn = Boolean.parseBoolean(elements[3]);
    }

    /**
     * Serializes the current attribute into a coma-separated-value string
     *
     * @return The generated CSV string
     */
    @Override
    public String toCsvString() {
        return String.join(",", this.refCode.toString(), this.lastName,
                this.flightCode, this.checkedIn.toString());
    }
}
