package main.java.data.model;

import main.java.data.InvalidDataException;
import main.java.data.InvalidReferenceException;
import java.text.ParseException;

public class Flight implements DataClass {

    private Reference flightCode;

    private String carrier;
    private String destination;
    private Integer maxPassengers;
    private Integer maxBaggageWeight;
    private BaggageDimension maxBaggageDimension;
    private Integer departureTime;

    private Boolean departed = false;
    private Integer totalOnBoard = 0;
    private Integer totalCheckedIn = 0;
    private Float totalBaggageWeight = 0.f;
    private Float totalBaggageVolume = 0.f;
    private Float totalExtraFees = 0.f;

    /**
     * Gets the reference code of the flight
     *
     * @return Reference string
     */
    @Override
    public Reference getRef() {
        return this.flightCode;
    }

    /**
     * Gets the flight's destination
     *
     * @return Destination string
     */
    public String getDestination() {
        return this.destination;
    }

    /**
     * Gets the flight's carrier
     *
     * @return Carrier string
     */
    public String getCarrier() {
        return this.carrier;
    }

    /**
     * Gets the maximum number of passenger in the flight
     *
     * @return Max number of passengers
     */
    public Integer getMaxPassengers() {
        return this.maxPassengers;
    }

    /***
     * Get the number of passengers checked in the flight
     *
     * @return Number of passengers checked in
     */
    public Integer getTotalCheckedIn() { return this.totalCheckedIn; }

    /***
     * Get current baggage weight of the flight
     *
     * @return The current baggage weight
     */
    public Float getTotalBaggageWeight() { return this.totalBaggageWeight; }

    /***
     * Get current baggage volume of the flight
     *
     * @return The current baggage volume
     */
    public Float getTotalBaggageVolume() { return this.totalBaggageVolume; }

    /***
     * Get current extra fees for the flight
     *
     * @return The current extra fees
     */
    public Float getTotalExtraFees() { return this.totalExtraFees; }

    /**
     * Gets the maximum dimension of baggages for the flight
     *
     * @return BaggageDimension object
     */
    public BaggageDimension getMaxBaggageDimension() {
        return this.maxBaggageDimension;
    }

    /**
     * Gets the maximum weight of baggage for the flight
     *
     * @return Maximum weight
     */
    public Integer getMaxBaggageWeight() {
        return this.maxBaggageWeight;
    }

    /**
     * Gets the departure time in seconds
     *
     * @return Departure time
     */
    public Integer getDepartureTime() { return this.departureTime; }

    /**
     * Gets info. about whether this flight has already departed. Only one thread can access to this block at a time
     *
     * @return The departure flag
     */
    public Boolean isDeparted() {
        synchronized (this) {
            return this.departed;
        }
    }

    /**
     * Sets the departure flag. Synchronize the value among the threads
     */
    public void setDeparted() {
        synchronized (this) {
            this.departed = true;
        }
    }

    /**
     * Get the number of passengers on board
     *
     * @return Number of passengers on board
     */
    public Integer getTotalOnBoard() { return this.totalOnBoard; }

    /**
     * Updates the interval values when a passenger has checked in. Only one thread can add a passenger to this
     * flight at time because of the departure flag.
     *
     * @param baggageWeight     Weight of the baggage
     * @param baggageVolume     Volume of the baggage
     * @param extraFees         Potential extra fees
     * @return Whether it was possible to complete the action because of the departed flag
     */
    public boolean processCheckInPassenger(float baggageWeight, float baggageVolume, float extraFees) {
        // Only one thread can execute inside a Java code block synchronized on the same monitor object.
        // To prevent this.departed from being updated while executing this block
        // Double checking: synchronizes only the affected part with extra test
        if (!this.departed) {
            synchronized (this) {
                if (!this.departed) {
                    this.totalCheckedIn++;
                    this.totalBaggageWeight += baggageWeight;
                    this.totalBaggageVolume += baggageVolume;
                    this.totalExtraFees += extraFees;
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Adds a new passenger on board after having checked in and passed the security control
     *
     * @return Whether it was possible to complete the action because of the departed flag
     */
    public boolean addPassengerOnBoard() {
        // Double checking: synchronizes only the affected part with extra test
        if (!this.departed) {
            synchronized (this) {
                if (!this.departed) {
                    this.totalOnBoard++;
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Parses a CSV formatted line to instantiate the attributes of the flight object
     *
     * @param line The string to parse data from
     * @throws InvalidDataException The string is not correctly formatted
     */
    @Override
    public void fromCsvString(String line) throws InvalidDataException {
        String[] elements = line.split(",");
        if (elements.length < 7) throw new InvalidDataException("flight", "Invalid data format");
        try {
            this.flightCode = new Reference(this, elements[0]);
        } catch (InvalidReferenceException e) {
            throw new InvalidDataException("flight", e.getMessage());
        }

        this.destination = elements[1];
        this.carrier = elements[2];
        this.maxPassengers = Integer.parseInt(elements[3]);
        this.maxBaggageWeight = Integer.parseInt(elements[4]);
        try {
            this.maxBaggageDimension = BaggageDimension.parse(elements[5]);
        } catch (ParseException e) {
            throw new InvalidDataException("flight", e.getMessage());
        }
        this.departureTime = Integer.parseInt(elements[6]);
        this.departed = false;
    }

    /**
     * Serializes the current attribute into a coma-separated-value string
     *
     * @return The generated CSV string
     */
    @Override
    public String toCsvString() {
        return String.join(",", this.flightCode.toString(), this.destination, this.carrier,
                this.maxPassengers.toString(), this.maxBaggageWeight.toString(), this.maxBaggageDimension.toString(),
                this.departureTime.toString(),
                this.totalCheckedIn.toString(), this.totalBaggageVolume.toString(), this.totalBaggageWeight.toString(),
                this.totalExtraFees.toString());
    }
}
