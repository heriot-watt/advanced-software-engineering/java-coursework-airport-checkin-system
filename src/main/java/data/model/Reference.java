package main.java.data.model;

import main.java.data.InvalidReferenceException;

public class Reference {

    private final String ref;

    /**
     * Reference constructor
     *
     * @param ref Reference string
     */
    public Reference(String ref) {
        if (ref == null || ref.isEmpty()) throw new IllegalArgumentException("Empty reference.");
        this.ref = ref;
    }

    /**
     * Reference constructor (with verifications)
     *
     * @param obj Object for reference generation
     * @param ref Reference string
     * @param <E> Object extending {@link DataClass}
     * @throws InvalidReferenceException The reference string does not follow the rules
     */
    public <E extends DataClass> Reference(E obj, String ref) throws InvalidReferenceException {
        check(obj, ref);
        this.ref = ref;
    }

    /**
     * Check the validity of the reference
     *
     * @param obj Dataclass object
     * @param ref Reference to verify
     * @param <E> Object extending {@link DataClass}
     * @throws InvalidReferenceException The reference does no follow the rules
     */
    public static <E extends DataClass> void check(E obj, String ref) throws InvalidReferenceException {
        char firstChar;

        if (obj instanceof Booking) firstChar = 'B';
        else if (obj instanceof Flight) firstChar = 'F';
        else return;

        if (ref.length() != 8) throw new InvalidReferenceException("Wrong size for reference id (8).", ref);
        if (ref.charAt(0) != firstChar) throw new InvalidReferenceException("Wrong category for reference.", ref);
        if (!ref.matches("[A-Z]{4}\\d{4}")) throw new InvalidReferenceException("Wrong reference format.", ref);
    }

    /**
     * Serialize the reference
     *
     * @return Serialized reference
     */
    @Override
    public String toString() {
        return this.ref;
    }

    /**
     * Methods overwritten so that two reference instances with the same value are equals
     * @return Hash value
     */
    @Override
    public int hashCode() {
        return this.ref.hashCode();
    }

    /**
     * Test the equality of two references
     *
     * @param obj Other Reference object
     * @return Equality boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Reference)
            return this.ref.equals(obj.toString());
        else
            return false;
    }
}
