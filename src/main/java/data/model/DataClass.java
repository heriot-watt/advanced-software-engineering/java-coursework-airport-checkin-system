package main.java.data.model;

import main.java.data.InvalidDataException;

public interface DataClass {

    /**
     * Get the reference to identify the object
     *
     * @return A Reference object
     */
    Reference getRef();

    /**
     * Instantiate all attributes from a coma-separated-value string
     *
     * @param line The string to parse data from
     * @throws InvalidDataException The string could not be parsed due to a wrong format
     */
    void fromCsvString(String line) throws InvalidDataException;

    /**
     * Serializes the current attribute into a coma-separated-value string
     *
     * @return The generated CSV string
     */
    String toCsvString();
}
