package main.java.data.model;

import java.text.ParseException;

/**
 * Object representing the dimensions of an airplane baggage (HxWxD)
 */
public class BaggageDimension {
    public final float height;
    public final float width;
    public final float depth;

    public BaggageDimension(float height, float width, float depth) {
        this.height = height;
        this.width = width;
        this.depth = depth;
    }

    /**
     * Generate a BaggageDimension object from a string
     *
     * @param s String to parse
     * @return Generated object
     * @throws ParseException Invalid baggage dimension format
     */
    public static BaggageDimension parse(String s) throws ParseException {
        String[] sizes = s.split("[xX]");

        try {
            if (sizes.length != 3)
                throw new ParseException("Invalid baggage dimension format.", 0);
            return new BaggageDimension(Float.parseFloat(sizes[0]), Float.parseFloat(sizes[1]),
                    Float.parseFloat(sizes[2]));
        } catch (NumberFormatException e) {
            throw new ParseException(e.getMessage(), 0);
        }
    }

    /**
     * Return the volume of the baggage
     *
     * @return Volume of the baggage
     */
    public float getVolume() {
        return this.height * this.width * this.depth;
    }

    /**
     * Serialize the dimensions with the HxWxD format
     *
     * @return Serialized string
     */
    @Override
    public String toString() {
        return String.format("%02.0fx%02.0fx%02.0f", this.height, this.width, this.depth);
    }

    /**
     * Equals operator override for the BaggageDimension object
     *
     * @param obj Object to compare
     * @return True if objects are of the same dimensions
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BaggageDimension)
            return this.height == ((BaggageDimension) obj).height && this.width == ((BaggageDimension) obj).width
                    && this.depth == ((BaggageDimension) obj).depth;
        else
            return false;
    }

    /**
     * Hash of the serialized version of the object
     *
     * @return String containing the hash
     */
    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

}
