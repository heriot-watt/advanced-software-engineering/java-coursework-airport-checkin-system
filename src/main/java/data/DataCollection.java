package main.java.data;

import main.java.data.model.DataClass;
import main.java.data.model.Reference;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class DataCollection implements Iterable<DataClass> {
    protected final HashMap<Reference, DataClass> data = new HashMap<>();

    /**
     * Init a given DataCollection class from a file
     *
     * @param collection DataCollection object to initialize
     * @param filePath   Path of the file to read
     * @param <E>        Class which extends the DataCollection abstract class
     * @return Initialized collection
     */
    public static <E extends DataCollection> E init(E collection, String filePath) {
        ArrayList<String> lines;
        try {
            lines = CsvIO.read(filePath);
            collection.fromLines(lines);
        } catch (FileNotFoundException e) {
            System.err.println("Invalid file path: file '" + filePath + "' not found.");
        }
        return collection;
    }

    /**
     * Fill a collection with DataClass from an array of lines
     *
     * @param lines Lines containing the data to be de-serialized
     */
    protected void fromLines(List<String> lines) {
        for (String line : lines) {
            if (line.equals(this.getHeader()))
                continue;

            DataClass b = this.getNewInstance();

            try {
                b.fromCsvString(line);
                this.data.put(b.getRef(), b);
            } catch (InvalidDataException e) {
                System.err.println("Invalid line: " + line);
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     * Creates a new DataClass object
     *
     * @return DataClass object
     */
    protected abstract DataClass getNewInstance();

    /**
     * Get the header corresponding to the data model
     *
     * @return String containing the header
     */
    protected abstract String getHeader();

    /**
     * Generates an array of lines from the collection
     *
     * @return Array of serialized strings
     */
    public ArrayList<String> toLines() {
        ArrayList<String> lines = new ArrayList<>();

        lines.add(getHeader());

        for (DataClass elem : this.data.values()) {
            lines.add(elem.toCsvString());
        }
        return lines;
    }

    /**
     * Find a given object in the collection
     *
     * @param key Key to find the given object
     * @return Found object (Null if not found)
     */
    public DataClass find(Reference key) {
        return this.data.get(key);
    }

    /**
     * Iterates over the objects in the collection (no specified order)
     *
     * @return Iterator over the collection
     */
    @Override
    public Iterator<DataClass> iterator() {
        return this.data.values().iterator();
    }
}
