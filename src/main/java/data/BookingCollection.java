package main.java.data;

import main.java.data.model.Booking;
import main.java.data.model.DataClass;
import main.java.data.model.Reference;

public class BookingCollection extends DataCollection {

    /**
     * Gets a new instance of a Booking object
     *
     * @return New Booking object
     */
    @Override
    protected Booking getNewInstance() {
        return new Booking();
    }

    /**
     * Get the CSV header of the Booking object
     *
     * @return CSV formatted header string
     */
    @Override
    protected String getHeader() {
        return "reference_code,last_name,flight_code,checked_in";
    }

    /**
     * Find a Booking in the collection
     *
     * @param key Key to find the booking
     * @return Found booking (Null if not found)
     */
    @Override
    public Booking find(Reference key) {
        return (Booking) super.find(key);
    }

    /**
     * Instantiates the references to the flight objects in the Booking objects
     *
     * @param coll The collection of flights
     */
    public void initFlights(FlightCollection coll) {
        for (DataClass d : this.data.values()) {
            Booking b = (Booking) d;
            b.flight = coll.find(new Reference(b.getFlightCode()));
        }
    }
}
