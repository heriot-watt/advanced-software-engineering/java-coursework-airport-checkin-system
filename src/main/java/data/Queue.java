package main.java.data;

import main.java.data.model.Booking;
import main.java.data.model.DataClass;
import main.java.observer.Observer;
import main.java.observer.Subject;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

public class Queue implements Iterable<Booking>, Subject {

    private final LinkedList<Booking> queue;
    private final LinkedList<Observer> observers;

    public Queue() {
        this.queue = new LinkedList<>();
        this.observers = new LinkedList<>();
    }

    /***
     * Instantiate a Queue from a BookingCollection
     *
     * @param bookings A BookingCollection
     */
    public Queue(BookingCollection bookings) {
        this();

        for (DataClass booking : bookings) {
            this.queue.add((Booking) booking);
        }
    }

    /***
     * Add an observer to the queue
     *
     * @param observer An observer
     */
    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    /***
     * Remove an observer from the queue
     *
     * @param observer An observer
     */
    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    /***
     * Update all the observers of the queue
     */
    @Override
    public void notifyObservers() {
        for (Observer observer : this.observers) {
            observer.update();
        }
    }

    /***
     * Synchronized method that returns the size of the queue
     *
     * @return The size of the queue
     */
    public synchronized int size() {
        return queue.size();
    }

    /***
     * Synchronized method that returns whether the queue is empty or not
     *
     * @return Whether the queue is empty or not
     */
    public synchronized Boolean hasNext() {
        return queue.size() > 0;
    }

    /***
     * Synchronized method that adds a new item at the end of the queue
     *
     * @param item Item to be added
     */
    public synchronized void add(Booking item) {
        this.queue.add(item);
        this.notifyObservers();
    }

    /***
     * Synchronized method that takes the first item from the queue
     *
     * @return The first item in the queue
     */
    public synchronized Booking getNext() {
        if (queue.isEmpty()) return null;
        Booking item = queue.removeFirst();
        this.notifyObservers();
        return item;
    }

    @Override
    public synchronized Iterator<Booking> iterator() {
        return queue.iterator();
    }
}
