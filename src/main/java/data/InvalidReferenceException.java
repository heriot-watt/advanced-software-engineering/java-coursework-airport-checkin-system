package main.java.data;

public class InvalidReferenceException extends Exception {

    /**
     * The reference does not follow the rules
     *
     * @param msg Message
     * @param ref Reference
     */
    public InvalidReferenceException(String msg, String ref) {
        super("Reference error on '" + ref + "': " + msg);
    }
}
