package main.java.observer;

import main.java.data.model.Booking;

public interface ObserverPull {
    void update(Booking booking);
}
