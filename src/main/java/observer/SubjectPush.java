package main.java.observer;

import main.java.data.model.Booking;

public interface SubjectPush {
    void registerObserver(ObserverPull observer);
    void removeObserver(ObserverPull observer);
    void notifyObservers(Booking booking);
}
