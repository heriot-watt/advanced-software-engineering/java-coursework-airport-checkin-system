package main.java.gui;

public class GUIWindowNullException extends RuntimeException {

    /**
     * GUI Controller accessed without instantiation
     */
    public GUIWindowNullException() {
        super("GUI Controller instance accessed without being instantiated");
    }
}
