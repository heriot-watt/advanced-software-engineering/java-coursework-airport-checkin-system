package main.java.gui.desks;

import main.java.data.model.Booking;
import main.java.gui.GUIWindow;
import main.java.gui.TextPane;
import main.java.observer.Observer;
import main.java.threads.CheckinDesk;

public class DeskPane extends TextPane implements Observer {

    private final CheckinDesk desk;
    private final Thread thread;
    private final int rank;

    /***
     * Pane displaying a desk information
     * Linked to a thread handling the checkin (checkin queue -> process (timeout) -> security queue)
     *
     * @param rank The desk number
     */
    public DeskPane(int rank) {
        GUIWindow guiWindow = GUIWindow.getInstance();

        // One thread per desk: takes a booking from checkinQueue, then forward it to securityQueue after timeout
        this.desk = new CheckinDesk(guiWindow.getCheckinQueue(), guiWindow.getSecurityQueue());
        this.desk.registerObserver(guiWindow.getFlightsPanel());
        this.desk.registerObserver(this);

        this.thread = new Thread(desk);
        this.thread.start();
        this.rank = rank;
    }

    /***
     * Stopping the checkin thread before closing the desk
     */
    public void closeDesk() {
        this.thread.interrupt();
    }

    /***
     * Update the Desk information to display
     */
    @Override
    public void update() {
        Booking booking = this.desk.getBooking();
        StringBuilder builder = new StringBuilder();

        // If null, then the desk is empty
        if (booking != null) {
            builder.append(String.format("Mr/Ms %s is dropping off 1 bag of %dkg.\n", booking.getLastName(), booking.getWeight()));
            if (booking.getExtraFees() > 0) builder.append(String.format("A baggage fee of £%.2f is due.\n", booking.getExtraFees()));
            else builder.append("No baggage fee is due.\n");
        } else {
            builder.append("Desk is empty.\n");
        }

        this.setTitle(String.format("Desk %d\n\n", rank));
        this.setContent(builder.toString());
    }
}
