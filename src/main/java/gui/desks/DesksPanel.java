package main.java.gui.desks;

import main.java.Logger;
import main.java.gui.Initialization;

import javax.swing.*;
import java.io.IOException;
import java.util.LinkedList;

public class DesksPanel extends JPanel implements Initialization {

    private final LinkedList<DeskPane> panes;

    /***
     * Panel displaying the list of desks
     */
    public DesksPanel() {
        this.panes = new LinkedList<>();
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    }

    /***
     * Finish initialization from constructor
     */
    @Override
    public void init() {
        this.addDesk();
    }

    /***
     * Open a desk for checkin
     */
    public void addDesk() {
        if (this.panes.size() < 4) {
            // The pane will be linked to a thread handling the checkin
            DeskPane pane = new DeskPane(this.panes.size());
            this.panes.add(pane);
            this.add(pane);

            try {
                Logger.getInstance().write(Logger.Sender.AIRPORT, "Opened desk %d", this.panes.size());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /***
     * Close a desk for checkin
     */
    public void removeDesk() {
        if (this.panes.size() > 0) {
            this.panes.getLast().closeDesk();
            this.remove(this.panes.removeLast());
            updateUI();

            try {
                Logger.getInstance().write(Logger.Sender.AIRPORT, "Closed desk %d", this.panes.size() + 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
