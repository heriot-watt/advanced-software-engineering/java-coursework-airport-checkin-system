package main.java.gui;

import main.java.Logger;
import main.java.data.*;
import main.java.data.Queue;
import main.java.gui.desks.DesksPanel;
import main.java.gui.flights.FlightsPanel;
import main.java.gui.queues.CheckinPanel;
import main.java.gui.queues.SecurityPanel;
import main.java.threads.PassengersArrival;
import main.java.threads.SecurityControl;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import static java.lang.System.exit;

public final class GUIWindow extends JFrame implements KeyListener, Initialization {

    private static GUIWindow instance;

    private final CheckinPanel checkinPanel;
    private final DesksPanel desksPanel;
    private final SecurityPanel securityPanel;
    private final FlightsPanel flightsPanel;

    private final Queue checkinQueue;
    private final Queue securityQueue;

    private final BookingCollection bookings;
    private final FlightCollection flights;

    private GUIWindow(BookingCollection bookings, FlightCollection flights) {
        this.bookings = bookings;
        this.flights = flights;

        // Instantiate the queues (checkin & security)
        this.checkinQueue = new Queue();
        this.securityQueue = new Queue();

        // Instantiate all the panels
        this.checkinPanel = new CheckinPanel();
        this.desksPanel = new DesksPanel();
        this.securityPanel= new SecurityPanel();
        this.flightsPanel = new FlightsPanel();

        // Set the layout of the application window (this)
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        this.pack();
        this.addKeyListener(this);
        this.setSize(600, 600);
        this.setFocusable(true);
        this.setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                generateReport(flights);
                try {
                    Logger.getInstance().write(Logger.Sender.PROGRAM, "Application closed");
                    Logger.getInstance().closeLog();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                exit(0);
            }
        });
    }

    /***
     * Finish initialization from constructor
     */
    @Override
    public void init() {
        // Finish initialization for all the panel using window attributes
        this.checkinPanel.init();
        this.desksPanel.init();
        this.securityPanel.init();
        this.flightsPanel.init();

        // Adding the 4 panels to window
        this.add(this.checkinPanel);
        this.add(this.desksPanel);
        this.add(this.securityPanel);
        this.add(this.flightsPanel);

        // One thread to simulate passengers arrival in airport
        PassengersArrival passengersArrival = new PassengersArrival(bookings, this.checkinQueue);
        Thread checkinThread = new Thread(passengersArrival);
        checkinThread.start();

        // One thread for boarding passengers once they passed the security control
        SecurityControl securityControl = new SecurityControl(this.securityQueue);
        Thread SecurityThread = new Thread(securityControl);
        // Push/Pull Observer pattern to update a specific flight when people pass the security
        securityControl.registerObserver(this.flightsPanel);
        SecurityThread.start();
    }

    /**
     * Instantiate the GUI once (singleton)
     *
     * @param bookings a collection of the bookings
     * @param flights a collection of the flights
     */
    public static void spawn(BookingCollection bookings, FlightCollection flights) {
        if (instance == null) {
            instance = new GUIWindow(bookings, flights);
            instance.init();
        }
    }

    /***
     * Getter for the GUIWindow instance
     * Warning: GUIWindow need to be instantiated by 'spawn' first
     *
     * @return The GUIWindow instance
     */
    public static GUIWindow getInstance() throws GUIWindowNullException {
        if (GUIWindow.instance == null) throw new GUIWindowNullException();
        return GUIWindow.instance;
    }

    /***
     * Getter for the Bookings collection
     *
     * @return The Bookings collection
     */
    public BookingCollection getBookings() {
        return this.bookings;
    }

    /***
     * Getter for the Flights collection
     *
     * @return The Flights collection
     */
    public FlightCollection getFlights() {
        return this.flights;
    }

    /***
     * Getter for Checkin queue
     *
     * @return The Checkin queue
     */
    public Queue getCheckinQueue() {
        return this.checkinQueue;
    }

    /***
     * Getter for Security queue
     *
     * @return The Security queue
     */
    public Queue getSecurityQueue() {
        return this.securityQueue;
    }

    /***
     * Getter for Checkin panel
     *
     * @return The Checkin panel
     */
    public CheckinPanel getCheckinPanel() {
        return this.checkinPanel;
    }

    /***
     * Getter for Desks panel
     *
     * @return The Desks panel
     */
    public DesksPanel getDesksPanel() {
        return this.desksPanel;
    }

    /***
     * Getter for Security panel
     *
     * @return The Security panel
     */
    public SecurityPanel getSecurityPanel() {
        return this.securityPanel;
    }

    /***
     * Getter for Flights panel
     *
     * @return The Flights panel
     */
    public FlightsPanel getFlightsPanel() {
        return this.flightsPanel;
    }

    /***
     * Keyboard event handler (add / remove desk)
     *
     * @param e Key event information
     */
    @Override
    public void keyTyped(KeyEvent e) {
        switch (e.getKeyChar()) {
            case 'v':
                this.desksPanel.addDesk();
                break;
            case 'b':
                this.desksPanel.removeDesk();
                break;
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    /**
     * Generates a report including all relevant info. about the flights.
     *
     * @param flights a collection of the flights existing in the application
     */
    private void generateReport(FlightCollection flights) {
        try {
            CsvIO.write(flights.toLines(), "report.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
