package main.java.gui.flights;

import main.java.data.model.Booking;
import main.java.data.model.DataClass;
import main.java.data.model.Flight;
import main.java.data.model.Reference;
import main.java.gui.GUIWindow;
import main.java.gui.Initialization;
import main.java.observer.ObserverPull;

import javax.swing.*;
import java.util.ArrayList;

public final class FlightsPanel extends JPanel implements ObserverPull, Initialization {

    private final ArrayList<FlightPane> panes;

    /***
     * Panel displaying all the flights information
     */
    public FlightsPanel() {
        this.panes = new ArrayList<>();
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    }

    /***
     * Finish initialization from constructor
     */
    @Override
    public void init() {
        for (DataClass flight: GUIWindow.getInstance().getFlights()) {
            FlightPane pane = new FlightPane((Flight) flight);
            this.panes.add(pane);
            this.add(pane);
        }
    }

    /***
     * Update a FlightPane given a flight information
     *
     * @param booking Booking furnished by thread (push/pull observer pattern)
     */
    @Override
    public synchronized void update(Booking booking) {
        Reference flightRef = booking.flight.getRef();

        for (FlightPane pane : this.panes) {
            if (pane.getFlightRef().equals(flightRef)) pane.update();
        }
    }
}
