package main.java.gui.flights;

import main.java.data.model.Flight;
import main.java.data.model.Reference;
import main.java.gui.TextPane;
import main.java.observer.Observer;
import main.java.threads.FlightDeparture;

public class FlightPane extends TextPane implements Observer {

    private final Flight flight;

    /***
     * Pane displaying a flight's information
     *
     * @param flight Flight information to display
     */
    public FlightPane(Flight flight) {
        this.flight = flight;
        this.update();

        // Start a thread for the flight departure and register to it
        FlightDeparture flightDeparture = new FlightDeparture(flight);
        Thread departureThread = new Thread(flightDeparture);
        flightDeparture.registerObserver(this);
        departureThread.start();
    }

    /***
     * Get the flight reference to which this pane is linked to
     */
    public Reference getFlightRef() {
        return this.flight.getRef();
    }

    /***
     * Update the content of the pane with the flight information
     */
    @Override
    public synchronized void update() {
        StringBuilder builder = new StringBuilder();

        if (this.flight.isDeparted()) builder.append("(Departed)\n\n");
        builder.append(String.format("%d checked in\n", this.flight.getTotalCheckedIn()));
        builder.append(String.format("%d on board of %d\n", this.flight.getTotalOnBoard(), this.flight.getMaxPassengers()));
        builder.append(String.format("Hold is %d%% full\n", (int) ((this.flight.getTotalBaggageWeight() / (this.flight.getMaxBaggageWeight() * this.flight.getMaxPassengers())) * 100)));

        this.setTitle(String.format("%s %s\n\n", this.flight.getRef(), this.flight.getDestination()));
        this.setContent(builder.toString());
    }
}
