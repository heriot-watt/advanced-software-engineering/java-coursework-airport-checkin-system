package main.java.gui;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;

public class TextPane extends JTextPane {

    private String title;
    private String content;
    // Whether to center the content or not (title is always centered)
    private final boolean center;

    public TextPane(boolean center) {
        this.title = "";
        this.content = "";
        this.center = center;

        setBorder(new LineBorder(new Color(100,100,100), 1));
        setFocusable(false);
        setEditable(false);
    }

    public TextPane() {
        this(true);
    }

    /***
     * Set the text of the pane to an empty string
     */
    private void resetPaneText() {
        this.setText("");
    }

    /***
     * Concatenate a given text to the current text of the pane
     *
     * @param text The text to insert
     * @param bold Whether to text should be bold or not
     */
    private void addPaneText(String text, Boolean bold) {
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyledDocument doc = this.getStyledDocument();
        int pos = doc.getLength();

        StyleConstants.setBold(attributeSet, bold);
        if (bold || this.center) StyleConstants.setAlignment(attributeSet, StyleConstants.ALIGN_CENTER);
        else StyleConstants.setAlignment(attributeSet, StyleConstants.ALIGN_LEFT);

        try {
            doc.insertString(doc.getLength(), text, attributeSet);
            doc.setParagraphAttributes(pos, doc.getLength() - pos, attributeSet, false);
        } catch (BadLocationException ignored) { }
    }

    /***
     * Set the title of the pane (bold text)
     *
     * @param title Title of the pane
     */
    public void setTitle(String title) {
        this.title = title;
        this.update();
    }

    /***
     * Set the text content of the pane (normal text beneath the title)
     *
     * @param content Text content of the pane
     */
    public void setContent(String content) {
        this.content = content;
        this.update();
    }

    /***
     * Refresh the pane with a title (bold) and a content
     */
    private void update() {
        SwingUtilities.invokeLater(() -> {
            this.resetPaneText();
            this.addPaneText(this.title, true);
            this.addPaneText(this.content, false);
        });
    }
}
