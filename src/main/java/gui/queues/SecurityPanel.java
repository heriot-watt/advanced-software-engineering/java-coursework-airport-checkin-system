package main.java.gui.queues;

import main.java.data.model.Booking;
import main.java.gui.GUIWindow;

public class SecurityPanel extends QueuePanel {

    /***
     * Initialize the queue attribute with the checkinQueue from GUIWindow
     */
    @Override
    protected void initQueue() {
        this.queue = GUIWindow.getInstance().getSecurityQueue();
    }

    /***
     * Get the name of the queue (Checkin / Security)
     */
    @Override
    protected String getQueueName() {
        return "Security";
    }

    /***
     * Format a booking into a string of the list display
     *
     * @param booking Booking details
     */
    @Override
    protected String formatBooking(Booking booking) {
        return String.format("  * %s\t%s\n", booking.getRef(), booking.getLastName());
    }
}
