package main.java.gui.queues;

import main.java.data.model.Booking;
import main.java.data.Queue;
import main.java.gui.Initialization;
import main.java.gui.TextPane;
import main.java.observer.Observer;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public abstract class QueuePanel extends JPanel implements Observer, Initialization {

    private final TextPane content;
    protected Queue queue;

    /***
     * Panel displaying a booking queue as a list
     */
    public QueuePanel() {
        this.content = new TextPane(false);
        this.content.setEditable(false);

        // Scrolling pane
        JScrollPane pane = new JScrollPane(this.content);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        pane.getVerticalScrollBar().setValue(0);

        // Panel settings
        this.setLayout(new BorderLayout());
        this.add(pane, BorderLayout.CENTER);
        this.setPreferredSize(new Dimension(600, 100));
        this.setBorder(new LineBorder(new Color(50, 50, 50), 1));
        this.setFocusable(false);
    }

    /***
     * Finish initialization from constructor
     */
    @Override
    public void init() {
        this.initQueue();
        this.queue.registerObserver(this);
        this.update();
    }

    /***
     * Initialize the queue attribute
     */
    protected abstract void initQueue();

    /***
     * Get the name of the queue (Checkin / Security)
     */
    protected abstract String getQueueName();

    /***
     * Format a booking into a string of the list display
     *
     * @param booking Booking details
     */
    protected abstract String formatBooking(Booking booking);

    /***
     * Update the list of people waiting
     */
    @Override
    public void update() {
        StringBuilder builder = new StringBuilder();

        for (Booking booking : this.queue) {
            builder.append(this.formatBooking(booking));
        }

        this.content.setTitle(String.format("%s queue: %d people waiting.\n\n", this.getQueueName(), this.queue.size()));
        this.content.setContent(builder.toString());
        ((JScrollPane) this.content.getParent().getParent()).getVerticalScrollBar().setValue(0);
    }
}
