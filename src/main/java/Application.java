package main.java;

import main.java.data.BookingCollection;
import main.java.data.DataCollection;
import main.java.data.FlightCollection;
import main.java.gui.GUIWindow;

import java.io.IOException;

public class Application {
    public static void main(String[] args) {
        FlightCollection flights = DataCollection.init(new FlightCollection(), "flights.csv");
        BookingCollection bookings = DataCollection.init(new BookingCollection(), "bookings.csv");

        bookings.initFlights(flights);

        try {
            Logger.getInstance().write(Logger.Sender.PROGRAM, "Program started");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        GUIWindow.spawn(bookings, flights);
    }
}
