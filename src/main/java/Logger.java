package main.java;

import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Logger {

    private static Logger instance;
    private final BufferedWriter writer;

    private static final String RESOURCES_PATH = "src/main/resources/";
    private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

    /**
     * Instantiate the buffer writer for a log file
     * (private constructor)
     */
    private Logger() throws FileNotFoundException {
        FileOutputStream stream;
        try {
            stream = new FileOutputStream(RESOURCES_PATH + "log.txt");
        } catch (FileNotFoundException e) {
            stream = new FileOutputStream(new File("log.txt"));
        }
        this.writer = new BufferedWriter(new OutputStreamWriter(stream));
    }

    /**
     * Singleton pattern: lazy instantiation of the Logger
     */
    public static Logger getInstance() throws FileNotFoundException {
        // Synchronizes only the part that creates a new instance with an extra test
        if (instance == null) {
            synchronized (Logger.class) {
                if (instance == null)
                    instance = new Logger();
            }
        }

        return instance;
    }

    /**
     * Close the buffer writer
     */
    public synchronized void closeLog() throws IOException {
        this.writer.close();
    }

    /**
     * Get the string timestamp from current time
     */
    private String getTimestamp() {
        return sdf.format(new Timestamp(System.currentTimeMillis()));
    }

    public enum Sender {
        PROGRAM,
        AIRPORT,
        CHECKIN,
        CONTROL,
        ONBOARD,
        PROCESS,
        TOOLATE
    }

    /**
     * Write a message in the log file
     *
     * @param sender Inform which entity is sending the message
     * @param msg Message to log (formatted string)
     * @param args Arguments for the string (internal String.format)
     */
    public void write(Sender sender, String msg, Object... args) throws IOException {
        String message = (args.length > 0) ? String.format(msg, args) : msg;
        this.writer.write(String.format("%s - (%s) %s", this.getTimestamp(), sender.toString(), message));
        this.writer.newLine();
        this.writer.flush();
    }
}
