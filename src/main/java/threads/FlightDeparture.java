package main.java.threads;

import main.java.Logger;
import main.java.data.model.Flight;

import java.io.IOException;

public class FlightDeparture extends TaskHandler {

    private final Flight flight;

    public FlightDeparture(Flight flight) {
        this.flight = flight;
    }

    @Override
    public void run() {
        try {
            // Wait for the departure time
            Thread.sleep(this.flight.getDepartureTime() * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Then set the departure and notify the observers
        this.flight.setDeparted();
        this.notifyObservers();

        try {
            Logger.getInstance().write(Logger.Sender.AIRPORT,"Flight with code %s has departed.", this.flight.getRef());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
