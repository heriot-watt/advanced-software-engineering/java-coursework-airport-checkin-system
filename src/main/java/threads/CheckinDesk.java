package main.java.threads;

import main.java.Logger;
import main.java.data.Queue;
import main.java.data.model.Booking;

import java.io.IOException;

public class CheckinDesk extends TaskHandler {

    private final Queue checkinQueue;
    private final Queue securityQueue;
    private Booking booking;

    public CheckinDesk(Queue checkinQueue, Queue securityQueue) {
        this.checkinQueue = checkinQueue;
        this.securityQueue = securityQueue;
        this.booking = null;
    }

    /**
     * Checkin and transfer the passenger to security control
     */
    private void checkIn() {
        this.booking.setCheckedIn();

        try {
            if (this.booking.flight.processCheckInPassenger(this.booking.getWeight(), this.booking.getDimension().getVolume(), this.booking.getExtraFees())) {
                Logger.getInstance().write(Logger.Sender.CONTROL, "%s finished check-in and joins the queue for the security control.", this.booking.getLastName());
                // Transfer the passenger to the security queue
                this.securityQueue.add(this.booking);
                // Notify that the checkin for this booking has been processed
                this.notifyObservers(this.booking);
            } else {
                Logger.getInstance().write(Logger.Sender.TOOLATE, "%s could not finish check-in because flight %s has already departed.", this.booking.getLastName(), this.booking.flight.getRef());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.booking = null;
    }

    public Booking getBooking() {
        return this.booking;
    }

    @Override
    public void run() {
        boolean flagEmpty = false, interrupted = false;

        while(!interrupted) {
            if (this.checkinQueue.hasNext()) {
                // Synchronization with double checking
                synchronized (this.checkinQueue) {
                    if (this.checkinQueue.hasNext()) {
                        // Process the next passenger for checkin
                        this.booking = this.checkinQueue.getNext();
                        flagEmpty = false;
                    }
                }
            }

            // Notify observers (only once if the desk was empty and still is)
            if (!flagEmpty) {
                // Notify the observers that the desk is processing a booking
                // Observers can access the booking from the getter
                this.notifyObservers();
                // If desk was empty
                flagEmpty = this.booking == null;
            }

            // If the desk is processing a booking
            if (this.booking != null) {
                try {
                    if (this.booking.flight != null && this.booking.flight.isDeparted()) {
                        Thread.sleep(1_500);
                        Logger.getInstance().write(Logger.Sender.TOOLATE, String.format("Flight with code %s has already departed for passenger %s.", this.booking.flight.getRef(), this.booking.getLastName()));
                    } else {
                        // The desk takes 5 seconds to process the checkin
                        Thread.sleep(5_000);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    interrupted = true;
                    // Immediate checkin when desk is closing
                } finally {
                    // Checkin the passenger and transfer it to the security queue
                    this.checkIn();
                }
            } else {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
    }
}
