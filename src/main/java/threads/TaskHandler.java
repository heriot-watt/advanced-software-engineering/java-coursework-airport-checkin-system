package main.java.threads;

import main.java.data.model.Booking;
import main.java.observer.Observer;
import main.java.observer.ObserverPull;
import main.java.observer.Subject;
import main.java.observer.SubjectPush;

import java.util.LinkedList;

public abstract class TaskHandler implements Runnable, Subject, SubjectPush {

    private final LinkedList<Observer> observers;
    private final LinkedList<ObserverPull> observersPull;

    public TaskHandler() {
        this.observers = new LinkedList<>();
        this.observersPull = new LinkedList<>();
    }

    /***
     * Add an observer to the queue
     *
     * @param observer An observer
     */
    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    /***
     * Add a pull observer to the queue
     *
     * @param observer A pull observer
     */
    @Override
    public void registerObserver(ObserverPull observer) {
        this.observersPull.add(observer);
    }

    /***
     * Remove an observer from the queue
     *
     * @param observer An observer
     */
    @Override
    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    /***
     * Remove a pull observer from the queue
     *
     * @param observer A pull observer
     */
    @Override
    public void removeObserver(ObserverPull observer) {
        this.observersPull.remove(observer);
    }

    /***
     * Update all the observers of the queue
     */
    @Override
    public void notifyObservers() {
        for (Observer observer : this.observers) {
            observer.update();
        }
    }

    /***
     * Update all the observers of the queue by pushing a booking information
     */
    @Override
    public void notifyObservers(Booking booking) {
        for (ObserverPull observer : this.observersPull) {
            observer.update(booking);
        }
    }
}
