package main.java.threads;

import main.java.Logger;
import main.java.data.Queue;
import main.java.data.model.Booking;

import java.io.IOException;

public class SecurityControl extends TaskHandler {

    private final Queue securityQueue;

    public SecurityControl(Queue securityQueue) {
        this.securityQueue = securityQueue;
    }

    /**
     * Board a passenger after he passed the security control
     *
     * @param booking Booking information
     */
    private void boarding(Booking booking) {
            try {
                if (booking.flight.addPassengerOnBoard()) {
                    Logger.getInstance().write(Logger.Sender.ONBOARD, "Passenger %s with flight %s is on board.", booking.getLastName(), booking.flight.getRef());
                    // Notify the observers of passenger boarding
                    this.notifyObservers(booking);
                } else {
                    Logger.getInstance().write(Logger.Sender.TOOLATE, "Passenger %s could not get on board because flight %s has already departed.", booking.getLastName(), booking.flight.getRef());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            if (this.securityQueue.hasNext()) {
                try {
                    // It takes 3 seconds to pass the security control
                    Thread.sleep(3_000);
                    this.boarding(this.securityQueue.getNext());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            } else {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
    }
}
