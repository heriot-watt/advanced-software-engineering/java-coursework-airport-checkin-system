package main.java.threads;

import main.java.Logger;
import main.java.data.BookingCollection;
import main.java.data.Queue;
import main.java.data.model.Booking;
import main.java.data.model.DataClass;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

public class PassengersArrival extends TaskHandler {

    private final LinkedList<Booking> pendingBookings;
    private final Queue checkinQueue;

    public PassengersArrival(BookingCollection bookings, Queue checkinQueue) {
        this.pendingBookings = new LinkedList<>();
        this.checkinQueue = checkinQueue;

        for (DataClass d : bookings) {
            pendingBookings.add((Booking) d);
        }

        // Shuffle the passengers queue
        Collections.shuffle(this.pendingBookings);
    }

    @Override
    public void run() {
        Random rand = new Random();

        while (!Thread.currentThread().isInterrupted()) {
            if (!this.pendingBookings.isEmpty()) {
                // Simulate a passenger arrival in the airport
                Booking booking = this.pendingBookings.removeFirst();
                this.checkinQueue.add(booking);

                try {
                    Logger.getInstance().write(Logger.Sender.CHECKIN, "%s joined the queue for the check-in.", booking.getLastName());
                    // Random timeout between 1 to 4 sec before next passenger arrival
                    Thread.sleep((rand.nextInt(3) + 1) * 1000);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            } else {
                break;
            }
        }
    }
}
