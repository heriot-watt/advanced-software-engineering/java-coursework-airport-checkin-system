package main.java.data.model;

import main.java.data.InvalidDataException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {

    private final String data = "BAFR5567,Doe,FBLN2445,false";
    private Booking booking;

    @BeforeEach
    public void setUp() {
        this.booking = new Booking();

        try {
            this.booking.fromCsvString(this.data);
        } catch (InvalidDataException e) {
            fail("Should not have failed booking initialization with '" + this.data + "'.");
        }
    }

    @Test
    public void setCheckedIn() {
        assertFalse(this.booking.getCheckedIn(), "Method getCheckedIn invalid. Should return false by default.");

        this.booking.setCheckedIn();
        assertTrue(this.booking.getCheckedIn(), "Method setCheckedIn invalid. Should return true when checked-in.");
    }

    @Test
    public void fromCsvStringExceptions() {
        assertThrows(InvalidDataException.class, () -> new Booking().fromCsvString("BAFR5567,Doe,FBLN2445"),
                "Should have thrown exception, the number of elements should be 4.");
        assertThrows(InvalidDataException.class, () -> new Booking().fromCsvString("FBLN2445,Doe,FBLN2445,false"),
                "Should have thrown exception, the booking reference is encoded as a flight code reference.");
    }

    @Test
    public void fromCsvString() {
        String[] elements = this.data.split(",");

        assertEquals(elements[0], this.booking.getRef().toString(),
                "Method fromCsvString invalid. The booking reference should be '" + elements[0]
                        + "', but found '" + this.booking.getRef().toString() + "' instead.");
        assertEquals(elements[1], this.booking.getLastName(),
                "Method fromCsvString invalid. The last name should be '" + elements[1]
                        + "', but found '" + this.booking.getLastName() + "' instead.");
        assertEquals(elements[2], this.booking.getFlightCode(),
                "Method fromCsvString invalid. The flight code should be '" + elements[2]
                        + "', but found '" + this.booking.getFlightCode() + "' instead.");
        assertEquals(Boolean.parseBoolean(elements[3]), this.booking.getCheckedIn(),
                "Method fromCsvString invalid. The checked in value should be '" + elements[3]
                        + "', but found '" + this.booking.getCheckedIn() + "' instead.");
    }

    @Test
    public void toCsvString() {
        assertEquals(this.data, this.booking.toCsvString(), "Method toCsvString is invalid, should return '"
                + this.data + "', but returned '" + this.booking.toCsvString() + "' instead.");
    }
}