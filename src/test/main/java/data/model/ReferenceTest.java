package main.java.data.model;

import main.java.data.InvalidReferenceException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ReferenceTest {

    private final String bookingRef = "BAFR5567";
    private final String flightRef = "FBLN2445";

    @Test
    public void getRef() {
        String str = new Reference(this.bookingRef).toString();
        assertEquals(this.bookingRef, str, "Method getRef is invalid, should return '" + this.bookingRef
                + "', but returned '" + str +"' instead.");
    }

    @Test
    public void check() {
        assertDoesNotThrow(() -> Reference.check(new Booking(), bookingRef),
                "Should not have thrown an exception for valid booking reference '"+ bookingRef +"'");
        assertDoesNotThrow(() -> Reference.check(new Flight(), flightRef),
                "Should not have thrown an exception for valid flight reference '"+ flightRef +"'");

        assertThrows(InvalidReferenceException.class, () -> Reference.check(new Booking(), flightRef),
                "Should have thrown an error, checking flight reference for Booking.");
        assertThrows(InvalidReferenceException.class, () -> Reference.check(new Flight(), bookingRef),
                "Should have thrown an error, checking booking reference for Flight.");
        assertThrows(InvalidReferenceException.class, () -> Reference.check(new Booking(), "ABC"),
                "Should have thrown an error, reference contains 8 characters.");
        assertThrows(InvalidReferenceException.class, () -> Reference.check(new Booking(), "B88AA66"),
                "Should have thrown an error, reference contains 4 characters and 4 numbers.");
    }

    @Test
    public void testToString() {
        String str = new Reference(this.bookingRef).toString();
        assertEquals(this.bookingRef, str, "Method toString is invalid, should return '" + this.bookingRef
                + "', but returned '" + str +"' instead.");
    }
}