package main.java.data.model;

import org.junit.jupiter.api.Test;
import java.text.ParseException;
import static org.junit.jupiter.api.Assertions.*;

class BaggageDimensionTest {

    @Test
    public void getVolume() {
        BaggageDimension baggageDimension = new BaggageDimension(10, 10, 10);
        assertEquals(1000, baggageDimension.getVolume(),
                "Getter for volume is invalid. Dimensions '10x10x10' return a volume of "
                        + baggageDimension.getVolume() + " instead of 1000.");
    }

    @Test
    public void testToString() {
        BaggageDimension baggageDimension = new BaggageDimension(10, 20, 30);
        assertEquals("10x20x30", baggageDimension.toString(),
                "Method toString is invalid. Returned '"+ baggageDimension.toString()
                        + "' instead of '10x20x30'.");
    }

    @Test
    public void parse() {
        try {
            assertEquals(new BaggageDimension(10, 20, 30), BaggageDimension.parse("10x20x30"),
                    "Parsing of string '10x20x30' is invalid.");
        }
        catch (ParseException e) {
            fail("Parsing failed when it should not have, for dimensions '10x20x30'.");
        }

        assertThrows(ParseException.class, () -> BaggageDimension.parse("10x20"),
                "Parsing should have failed, but did not, for dimensions '10x20'.");
        assertThrows(ParseException.class, () -> BaggageDimension.parse("10xaax30"),
                "Parsing should have failed, but did not, for dimensions '10xaax30'.");
    }
}
