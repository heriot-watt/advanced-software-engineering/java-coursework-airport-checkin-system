package main.java.data.model;

import main.java.data.InvalidDataException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FlightTest {

    private final String data = "FBLN2445,Berlin,Ryanair,60,10,55x40x20,20";
    private Flight flight;

    @BeforeEach
    public void setUp() {
        this.flight = new Flight();

        try {
            this.flight.fromCsvString(this.data);
        } catch (InvalidDataException e) {
            fail("Should not have failed flight initialization with '" + this.data + "'.");
        }
    }

    @Test
    public void isDeparted() {
        assertFalse(this.flight.isDeparted(), "Method isDeparted invalid. Should return false by default.");

        this.flight.setDeparted();
        assertTrue(this.flight.isDeparted(), "Method isDeparted invalid. Should return true when departed.");
    }

    @Test
    public void setDeparted() {
        this.flight.setDeparted();
        assertTrue(this.flight.isDeparted(), "Method isDeparted invalid. Should return true when departed.");
    }

    @Test
    public void processCheckInPassenger() {
        int weight = 10;
        float volume = 111.2f;
        float extraFees = 21.3f;
        boolean valid;

        valid = this.flight.processCheckInPassenger(weight, volume, extraFees);

        assertTrue(valid, "Method processCheckInPassenger invalid. Should return true when flight has not departed.");

        this.flight.setDeparted();
        valid = this.flight.processCheckInPassenger(weight, volume, extraFees);

        assertFalse(valid, "Method processCheckInPassenger invalid. Should return false when flight has departed.");
        assertEquals(1, this.flight.getTotalCheckedIn(),
                "Method processCheckInPassenger invalid. Number of passenger checked-in should be '" + 1
                        + "', but found '" + this.flight.getTotalCheckedIn() + "' instead.");
        assertEquals(weight, this.flight.getTotalBaggageWeight(),
                "Method processCheckInPassenger invalid. Total baggage weight should be '" + weight
                        + "', but found '" + this.flight.getTotalBaggageWeight() + "' instead.");
        assertEquals(volume, this.flight.getTotalBaggageVolume(),
                "Method processCheckInPassenger invalid. Total baggage volume should be '" + weight
                        + "', but found '" + this.flight.getTotalBaggageVolume() + "' instead.");
        assertEquals(extraFees, this.flight.getTotalExtraFees(),
                "Method processCheckInPassenger invalid. Total extra fees should be '" + weight
                        + "', but found '" + this.flight.getTotalExtraFees() + "' instead.");
    }

    @Test
    public void addPassengerOnBoard() {
        boolean valid;

        valid = this.flight.addPassengerOnBoard();

        assertTrue(valid, "Method processCheckInPassenger invalid. Should return true when flight has not departed.");

        this.flight.setDeparted();
        valid = this.flight.addPassengerOnBoard();

        assertFalse(valid, "Method processCheckInPassenger invalid. Should return false when flight has departed.");
        assertEquals(1, this.flight.getTotalOnBoard(),
                "Method processCheckInPassenger invalid. Number of passengers on board should be '" + 1
                        + "', but found '" + this.flight.getTotalOnBoard() + "' instead.");
    }

    @Test
    public void fromCsvStringExceptions() {
        assertThrows(InvalidDataException.class,
                () -> new Flight().fromCsvString("FBLN2445,Berlin,Ryanair,60,10"),
                "Should have thrown exception, the number of elements should be 6.");
        assertThrows(InvalidDataException.class,
                () -> new Flight().fromCsvString("BAFR5567,Berlin,Ryanair,60,10,55x40x20,20"),
                "Should have thrown exception, the flight reference is encoded as a booking code reference.");
    }

    @Test
    public void fromCsvString() {
        String[] elements = this.data.split(",");

        assertEquals(elements[0], this.flight.getRef().toString(),
                "Method fromCsvString invalid. The flight reference should be '" + elements[0]
                        + "', but found '" + this.flight.getRef().toString() + "' instead.");
        assertEquals(elements[1], this.flight.getDestination(),
                "Method fromCsvString invalid. The destination should be '" + elements[1]
                        + "', but found '" + this.flight.getDestination() + "' instead.");
        assertEquals(elements[2], this.flight.getCarrier(),
                "Method fromCsvString invalid. The carrier should be '" + elements[2]
                        + "', but found '" + this.flight.getCarrier() + "' instead.");
        assertEquals(Integer.parseInt(elements[3]), this.flight.getMaxPassengers(),
                "Method fromCsvString invalid. The checked in value should be '" + elements[3]
                        + "', but found '" + this.flight.getMaxPassengers() + "' instead.");
        assertEquals(Integer.parseInt(elements[4]), this.flight.getMaxBaggageWeight(),
                "Method fromCsvString invalid. The flight code should be '" + elements[4]
                        + "', but found '" + this.flight.getCarrier() + "' instead.");
        assertEquals(elements[5], this.flight.getMaxBaggageDimension().toString(),
                "Method fromCsvString invalid. The flight code should be '" + elements[5]
                        + "', but found '" + this.flight.getMaxBaggageDimension() + "' instead.");
        assertEquals(Integer.parseInt(elements[6]), this.flight.getDepartureTime(),
                "Method fromCsvString invalid. The flight departure time should be '" + elements[6]
                        + "', but found '" + this.flight.getDepartureTime() + "' instead.");
    }

    @Test
    public void toCsvString() {
        String[] elements = this.flight.toCsvString().split(",");
        String str = String.join(",", Arrays.copyOfRange(elements, 0, 7));

        assertEquals(this.data, str, "Method toCsvString is invalid, should return '"
                + this.data + "', but returned '" + this.flight.toCsvString() + "' instead.");
        assertEquals(11, elements.length, "Method toCsvString is invalid, should return 10 elements.");

        for (String elt : Arrays.copyOfRange(elements, 6, elements.length)) {
            try {
                Float.parseFloat(elt);
            } catch (NumberFormatException e) {
                fail("Method toCsvString is invalid, the last 4 elements should be numbers.");
                break;
            }
        }
    }
}
