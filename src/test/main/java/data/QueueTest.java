package main.java.data;

import main.java.data.model.Booking;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class QueueTest {

    @Test
    void size() {
        Random rand = new Random();
        Queue queue = new Queue();
        int size = rand.nextInt(9) + 1;

        for (int i = 0; i < size; i++) {
            queue.add(new Booking());
        }

        assertEquals(size, queue.size(),
                "Method size invalid. The queue size should be '" + size
                        + "', but found '" + queue.size() + "' instead.");
    }

    @Test
    void hasNext() {
        Queue queue = new Queue();

        assertFalse(queue.hasNext(), "Method hasNext invalid. Should have returned false for empty queue.");

        queue.add(new Booking());
        assertTrue(queue.hasNext(), "Method hasNext invalid. Should have returned true with queue of size 1.");
    }

    @Test
    void add() {
        Queue queue = new Queue();
        Booking booking = new Booking();

        queue.add(booking);

        assertTrue(queue.hasNext(), "Method add invalid. Should have return true when added a booking.");
        assertEquals(1, queue.size(), "Method add invalid. Size of queue should be equal to 1 when adding a booking.");
        assertEquals(booking, queue.getNext(), "Method add invalid. Booking returned from getNext should return the booking added.");
    }

    @Test
    void getNext() {
        Queue queue = new Queue();
        Booking booking = new Booking();

        queue.add(booking);

        assertEquals(booking, queue.getNext(), "Method add invalid. Booking returned from getNext should return the booking added.");
        assertEquals(0, queue.size(), "Method add invalid. Size of queue should be 0 when getting last booking.");
        assertFalse(queue.hasNext(), "Method add invalid. Should have return false after getting last booking.");
    }
}
