package main.java.data;

import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CsvIOTest {

    private final List<String> data = Arrays.asList(
            "BAFR5567,Doe,FBLN2445,false",
            "BDFH6833,Smith,FBLN2445,false");

    private void test(ArrayList<String> list, String method) {
        assertEquals(this.data.size(), list.size(), "Method " + method + " invalid, was supposed to load "
                + this.data.size() + " elements, but only have " + list.size() + ".");

        for (int i = 0; i < this.data.size(); i++) {
            if (!list.get(i).equals(this.data.get(i))) {
                fail("Method " + method + " invalid, reading of the input stream is corrupted.");
                break;
            }
        }
    }

    @Test
    public void read() {
        ArrayList<String> list;
        StringBuilder sb = new StringBuilder();

        for (String str : this.data)
            sb.append(str).append('\n');

        list = CsvIO.read(new ByteArrayInputStream(sb.toString().getBytes(StandardCharsets.UTF_8)));

        test(list, "read");
    }

    @Test
    public void write() {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ArrayList<String> list;

        try {
            CsvIO.write(new ArrayList<>(this.data), os);
        } catch (IOException e) {
            fail("Method write invalid, something wrong happened while writing the data");
        }

        list = new ArrayList<>(Arrays.asList(os.toString(StandardCharsets.UTF_8).split("\n")));

        test(list, "write");
    }
}
