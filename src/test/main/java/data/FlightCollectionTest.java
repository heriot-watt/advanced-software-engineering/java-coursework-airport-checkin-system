package main.java.data;

import main.java.data.model.Flight;
import main.java.data.model.Reference;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightCollectionTest {

    private final List<String> data = Arrays.asList(
            "FBLN2445,Berlin,Ryanair,60,10,55x40x20,20",
            "FAMD6578,Amsterdam,Ryanair,50,10,55x40x20,30");
    private List<String> refs;

    @BeforeEach
    public void setUp() {
        this.refs = new ArrayList<>();

        for (String elt: this.data) {
            String[] elements = elt.split(",");
            this.refs.add(elements[0]);
        }
    }

    @Test
    public void init() throws IOException {
        String filePath = "flights.csv";
        FlightCollection flightCollection1 = new FlightCollection();
        FlightCollection flightCollection2 = DataCollection.init(new FlightCollection(), filePath);

        flightCollection1.fromLines(CsvIO.read(filePath));

        assertEquals(flightCollection1.data.size(), flightCollection2.data.size(),
                "Method init invalid, was supposed to load " + flightCollection1.data.size()
                        + " elements, but only have " + flightCollection2.data.size() + ".");
    }

    @Test
    public void fromLines() {
        FlightCollection flightCollection = new FlightCollection();
        Flight flight2, flight1 = new Flight();

        flightCollection.fromLines(this.data);

        assertEquals(this.data.size(), flightCollection.data.size(), "Method fromLines invalid, "
                + this.data.size() +" elements were given, but only " + flightCollection.data.size()
                + " were initialized.");

        for (int i = 0; i < this.data.size(); i++) {
            try {
                flight1.fromCsvString(this.data.get(i));
            } catch (Exception e) {
                fail("Something went wrong while testing method fromLines, could not parse valid data.");
            }

            flight2 = flightCollection.find(new Reference(this.refs.get(i)));

            assertEquals(flight1.getRef(), flight2.getRef(),
                    "Method fromLines invalid, initialization of flights is corrupted.");
        }
    }

    @Test
    public void toLines() {
        FlightCollection flightCollection = new FlightCollection();
        List<String> lines1, lines2 = new ArrayList<>();

        flightCollection.fromLines(this.data);

        lines1 = flightCollection.toLines();
        lines1.remove(0);

        for (String elt : lines1)
            lines2.add(String.join(",", Arrays.copyOfRange(elt.split(","), 0, 7)));

        assertEquals(this.data, lines2, "Method toLines is invalid,"
                + " should return same value used as input for the method fromLines.");
    }

    @Test
    public void find() {
        FlightCollection flightCollection = new FlightCollection();
        Flight flight;
        Reference ref;

        flightCollection.fromLines(this.data);

        for (String str : this.refs) {
            ref = new Reference(str);
            flight = flightCollection.find(ref);

            if (flight != null)
                assertEquals(ref, flight.getRef(), "Method find invalid, flight " + str
                        + " exists in collection, but it returned '" + flight.getRef() + "'");
            else
                fail("Method find invalid, could not find flight " + str + ".");
        }
    }

    @Test
    public void getNewInstance() {
        assertEquals(new Flight().getRef(), new FlightCollection().getNewInstance().getRef(),
                "Method getInstance invalid, should return a new instance of Flight.");
    }
}
