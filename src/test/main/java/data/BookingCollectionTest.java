package main.java.data;

import main.java.data.model.Booking;
import main.java.data.model.DataClass;
import main.java.data.model.Reference;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookingCollectionTest {

    private final List<String> data = Arrays.asList(
            "BAFR5567,Doe,FBLN2445,false",
            "BDFH6833,Smith,FBLN2445,false");
    private List<String> refs;

    @BeforeEach
    void setUp() {
        this.refs = new ArrayList<>();

        for (String elt: this.data) {
            String[] elements = elt.split(",");
            this.refs.add(elements[0]);
        }
    }

    @Test
    void init() throws IOException {
        String filePath = "bookings.csv";
        BookingCollection bookingCollection1 = new BookingCollection();
        BookingCollection bookingCollection2 = DataCollection.init(new BookingCollection(), filePath);

        bookingCollection1.fromLines(CsvIO.read(filePath));

        assertEquals(bookingCollection1.data.size(), bookingCollection2.data.size(),
                "Method init invalid, was supposed to load " + bookingCollection1.data.size()
                        + " elements, but only have " + bookingCollection2.data.size() + ".");
    }

    @Test
    void fromLines() {
        BookingCollection bookingCollection = new BookingCollection();
        Booking booking2, booking1 = new Booking();

        bookingCollection.fromLines(this.data);

        assertEquals(bookingCollection.data.size(), this.data.size(), "Method fromLines invalid, "
                + this.data.size() +" elements were given, but only " + bookingCollection.data.size()
                + " were initialized.");

        for (int i = 0; i < this.data.size(); i++) {
            try {
                booking1.fromCsvString(this.data.get(i));
            } catch (Exception e) {
                fail("Something went wrong while testing method fromLines, could not parse valid data.");
            }

            booking2 = bookingCollection.find(new Reference(this.refs.get(i)));

            assertEquals(booking1.getRef(), booking2.getRef(),
                    "Method fromLines invalid, initialization of bookings is corrupted.");
        }
    }

    @Test
    void toLines() {
        BookingCollection bookingCollection = new BookingCollection();
        List<String> lines;

        bookingCollection.fromLines(this.data);

        lines = bookingCollection.toLines();
        lines.remove(0);

        assertEquals(this.data, lines, "Method toLines is invalid,"
                + " should return same value used as input for the method fromLines.");
    }

    @Test
    void find() {
        BookingCollection bookingCollection = new BookingCollection();
        Booking booking;
        Reference ref;

        bookingCollection.fromLines(this.data);

        for (String str : this.refs) {
            ref = new Reference(str);
            booking = bookingCollection.find(ref);

            if (booking != null)
                assertEquals(ref, booking.getRef(), "Method find invalid, booking " + str
                        + " exists in collection, but it returned '" + booking.getRef() + "'");
            else
                fail("Method find invalid, could not find booking " + str + ".");
        }
    }

    @Test
    void getNewInstance() {
        assertEquals(new Booking().getRef(), new BookingCollection().getNewInstance().getRef(),
                "Method getInstance invalid, should return a new instance of Booking.");
    }

    @Test
    void initFlights() {
        FlightCollection flights = DataCollection.init(new FlightCollection(), "flights.csv");
        BookingCollection bookings = DataCollection.init(new BookingCollection(), "bookings.csv");

        bookings.initFlights(flights);

        for (DataClass booking : bookings.data.values()) {
            if (((Booking) booking).flight == null) {
                fail("Method initFlights invalid, could not map one of the booking with the flight.");
                break;
            }
        }
    }
}